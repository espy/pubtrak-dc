import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pubtrakdc",
    version="0.0.1",
    author="Stephen Espy",
    author_email="espy@espy.net",
    description="pubtrakdc",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/espy/pubtrak-dc",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)