#!/usr/bin/env bash

# -------------------- pre -------------------- #

NOM="pubtrakdc"

ORIGINAL_WD="${PWD}"
SCRIPT_PARENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${SCRIPT_PARENT_DIR}"
cd ..

# -------------------- work -------------------- #

pip3 install -r requirements.txt
./script/test.sh
rm -rf pubtrakdc/__pycache__
python ./setup.py sdist bdist_wheel
