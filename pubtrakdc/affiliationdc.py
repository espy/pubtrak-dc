from dataclasses import dataclass
import bson


string_index = {
        'affiliation_uuid':0,
        'is_in_scope':1,
        'is_curated':2,
        'canonical_name':3,
        'sub_name':4,
        'name':5
}


@dataclass
class AffiliationDC:
    """Class for keeping track of an institutional affiliation."""

    _id: bson.objectid.ObjectId = None
    name: str = ''
    canonical_name: str = ''
    sub_name: str = ''
    affiliation_uuid: str = ''
    inserted_dt: str = ''
    is_in_scope: int = 0
    is_curated: int = 0

# ---------- ephemeral fields ---------- #
    instance_string_index = None


    def isInScope(self) -> bool:
        if self.is_in_scope == 0:
            return False
        else:
            return True

    def asStringList(self):
        #TODO add ability to pass in a string index?

        if not self.instance_string_index:
            self.instance_string_index = string_index

        strings = [None] * len(self.instance_string_index.keys())

        strings[self.instance_string_index['affiliation_uuid']] = self.affiliation_uuid
        strings[self.instance_string_index['is_in_scope']] = self.is_in_scope
        strings[self.instance_string_index['is_curated']] = self.is_curated
        strings[self.instance_string_index['canonical_name']] = self.canonical_name
        strings[self.instance_string_index['sub_name']] = self.sub_name
        strings[self.instance_string_index['name']] = self.name

        strings.append(self._id)
        strings.append(self.name)
        strings.append(self.canonical_name)
        strings.append(self.sub_name)
        strings.append(self.inserted_dt)
        strings.append(self.is_in_scope)
        strings.append(self.is_curated)

        return strings


