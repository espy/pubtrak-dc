from dataclasses import dataclass
import bson


@dataclass
class AuthorDC:
    """Class for keeping track of an author."""

    _id: bson.objectid.ObjectId = None
    author_uuid = ''
    lname = ''
    fname = ''
    intials = ''
    orcid = ''
    curr_aff = ''
    affiliation_uuids = []
    inserted_dt: str = ''
